var loader = (loader = {})


loader.vm = (function(){
    var self = this;
    var util = utility.console();

    self.js = [];
    self.css = [];
    self.sprites = [];

    self.pushJs = function(model){
        self.js.push(model);
        util.write("Pushed " + model.url);
    }
    
    self.loadJSResources = function(){     
        for (var i = 0; i < self.js.length; i++){
            loadJs(self.js[i]);
        }
    }
    
    var loadJs = function(jsObj){
        var sT = document.createElement("script");
        sT.src = jsObj.url;
        sT.onload = function(){
            jsObj.isLoaded = true;
            util.write(jsObj.name +" Loaded");
            jsLoadedEvent();
        }
        document.body.appendChild(sT);
    }

    self.init = function(){
        //Push outside resources first.        
        //self.pushJs(new loader.model("https://code.jquery.com/jquery-3.2.1.slim.min.js"));

        //get files from resource
        getResourceList();
    }

    self.loadComplete = function(callback){
        loadCompleteEvent = callback;
    }

    var loadCompleteEvent =  function(){
        console.error("Not Implemented");
    }
    
    var jsLoadedEvent = function(){
        //Check if all js are loaded.
        var t = 0;
        for (var i = 0; i < self.js.length; i++){
            if (self.js[i].isLoaded) t++;
        }
        if (t === self.js.length){
            util.write("All resources loaded [" + t + "/"+self.js.length+"]");
            loadCompleteEvent();
        }
    }

    var getResourceList = function(){
        var request = new XMLHttpRequest();

        request.onreadystatechange = function(){
            if (request.readyState == XMLHttpRequest.DONE){
                var data = JSON.parse(request.responseText);
                for(var i = 0; i < data.js_files.length; i++){
                    self.pushJs(new loader.model(data.js_files[i].uri, data.js_files[i].name));
                }
            } else if (request.status == 400){
                 util.write(request.responseText);
            }
        };

        request.open("GET", "./js/helpers/res_list.json", false);
        request.send();
    }

    return {
        init: self.init,
        loadComplete: self.loadComplete,
        loadJSResources: self.loadJSResources,
        addResource: self.pushJs
    }
});

loader = (loader || {})

loader.model = function(url, name){
    var self = this;

    return {
        name: name != undefined && name != null ? name : url,
        url: url,
        isLoaded: false
    };
};