tileMap = (tileMap = {});

tileMap.map = function(width, height){
    var self = this;
    
    self.tileList = []; //resource list
    self.map = utility.functions().createArray(height, width); //[[]] 2d array of tiles.

    self.init = function(){
        for (var i = 0; i < height; i++){
            for (var j = 0; j < width; j++){
                self.map[i][j] = new tileMap.tile("a");
            }
        }
        console.log(self.map, screen.tilesX(), screen.tilesY(), camera.cam);
    }

    self.draw = function(ctx){
        for (var i = 0; i < height; i++){
            for (var j = 0; j < width; j++){
                if ((i >= camera.cam.x)
                    && (j >= camera.cam.y)){
                    self.map[i][j].draw(ctx, i, j);
                }
            }
        }
    }

    return {
        init: self.init,
        draw: self.draw
    }
};

tileMap.tile = function(tile_type){
    var self = this;
    
    self.tileType = tile_type; //Type of tile: IE => Dirt, Sand, Water, etc.
    self.w = 16;
    self.h = 16;

    self.draw = function(ctx, x, y){
        ctx.fillText(self.tileType,
         x * self.w ,
         y * self.h);
    }

    return {
        draw: self.draw,
        type: self.tileType,
        width: self.w,
        height: self.h
    }
}