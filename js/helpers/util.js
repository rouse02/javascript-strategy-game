utility = (utility = {});

utility.console = function(){
    var self = this;

    self.writeToConsole = function(input, isError = false){
        var t = document.getElementById("console_log");

        var result = "<p";
        result += isError ? " style='color: red'>" : ">";

        result += input.toString().replace("\n", "<br/>") + "</p>";
        var cur = t.innerHTML;
        
        t.innerHTML = (cur + result);
    }

    return {
        write: self.writeToConsole
    }
};

utility.functions = function(){
    var self = this;

    self.createArray = function(length){
        var arr = new Array(length || 0),
        i = length;

        if (arguments.length > 1) {
            var args = Array.prototype.slice.call(arguments, 1);
            while(i--) arr[length-1 - i] = self.createArray.apply(this, args);
        }

        return arr;
    }

    return {
        createArray: self.createArray
    };
};