var requestAnimFrame = (function(){
    return window.requestAnimationFrame       ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        window.oRequestAnimationFrame      ||
        window.msRequestAnimationFrame     ||
        function(callback){
            window.setTimeout(callback, 1000 / 60);
        };
})();

loader = (loader || {});
screen = {};

//Create Canvas
var canvas = document.getElementById("game");
var ctx = canvas.getContext("2d");
// canvas.width = 512;
// canvas.height = 480;

var map;
var fpsTracker = new performance();

var lastTime;
function main(){
    var now = Date.now();
    var dt = (now - lastTime) / 1000.0;

    update(dt);
    render(ctx);
    fpsTracker.updateFps(dt);
    lastTime = now;    
    requestAnimFrame(main);
}

function update(dt){
    //TODO: Update logic.
}

function render(ctx){
    //clear the screen for the next render
    ctx.clearRect(0,0, canvas.width, canvas.height);
    //TODO: Draw things.
    map.draw(ctx);
}

function postLoad(){    
    screen = new camera.screen(512, 480);
    map = new tileMap.map(512, 480);
    map.init();    
    main();
}

function init(){
    var load = loader.vm();
    
    load.init();
    lastTime = Date.now();
    //TODO: Add other files here.
    load.loadComplete(postLoad);
    load.loadJSResources();
    
    calculateGameArea();
}

function performance(){
    var self = this;

    self.updateFps = function(val){
        var t = 1000 / (val * 100)
        $("#fps").html("FPS: " + t.toFixed(1));
    }
}

function calculateGameArea(){
    var fontBase = 1000,
        fontSize = 70;
    
    var ratio = fontSize / fontBase;
    var size = canvas.width * ratio;
    ctx.font = (size|0) + 'px sans-serif';
}

init();