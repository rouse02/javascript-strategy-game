class Menu {
    items: Array<string>;
    pages: number;

    constructor(item_list: Array<string>, total_pages: number){
        this.items = item_list;
        this.pages = total_pages;
    }

    list(): void{
        console.log("Our menu for today:");
        for (var i = 0; i < this.items.length; i++){
            console.log(this.items[i]);
        }
    }
}

class ExtendMenu extends Menu {
    constructor(item_list: Array<string>, total_pages: number){
        super(item_list, total_pages);
    }

    list(): void{
        console.log("Extending the menu Yo!");
        for(var i = 0; i < this.items.length; i++){
            console.log("Extended: " + this.items[i]);
        }
    }
}

class FunctionHelpers {
    genericFunc<T>(argument: T): T[]{
        var arrayOfT: T[] = [];
        arrayOfT.push(argument);
        return arrayOfT;
    }
}

var sundayMenu = new Menu(["pan", "egg", "OJ"], 1);
var exMenu = new ExtendMenu(["pan", "egg", "OJ"], 1);
sundayMenu.list();
exMenu.list();