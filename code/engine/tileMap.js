"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tile_1 = require("./tile");
var TileMap = (function () {
    function TileMap(w, h) {
        this.width = w;
        this.height = h;
    }
    TileMap.prototype.init = function () {
        for (var i = 0; i < this.height; i++) {
            for (var j = 0; j < this.width; j++) {
                this.map[i][j] = new tile_1.Tile("a");
            }
        }
        console.log(this.map);
    };
    TileMap.prototype.draw = function (ctx, camera) {
        for (var i = 0; i < this.height; i++) {
            for (var j = 0; j < this.width; j++) {
                if ((i >= camera.position.x)
                    && (j >= camera.position.y)) {
                    this.map[i][j].draw(ctx, i, j);
                }
            }
        }
    };
    return TileMap;
}());
exports.TileMap = TileMap;
//# sourceMappingURL=tileMap.js.map