class Camera{
    width: number;
    height: number;
    position: cam;

    constructor(width: number, height: number){
        this.width = width;
        this.height = height;
        this.position.x = 0;
        this.position.y = 0;
    }

    tilesX(): number{
        return this.width / 16;
    }

    tilesY(): number{
        return this.height / 16;
    }    
}

export {Camera};

export interface cam {
    x: number;
    y: number;
}