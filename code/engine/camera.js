"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Camera = (function () {
    function Camera(width, height) {
        this.width = width;
        this.height = height;
        this.position.x = 0;
        this.position.y = 0;
    }
    Camera.prototype.tilesX = function () {
        return this.width / 16;
    };
    Camera.prototype.tilesY = function () {
        return this.height / 16;
    };
    return Camera;
}());
exports.Camera = Camera;
//# sourceMappingURL=camera.js.map