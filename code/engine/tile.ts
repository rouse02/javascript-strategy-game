class Tile {
    tileType: string; //TODO: Change when I have an actual type.
    width: number;
    height: number;

    constructor(tile_type: string){
        this.tileType = tile_type;
        this.width = 16;
        this.height = 16;
    }

    draw(ctx: CanvasRenderingContext2D, x: number, y: number):void{
        ctx.fillText(this.tileType, x * this.width, y * this.height);
    }
}

export {Tile}