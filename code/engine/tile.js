"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Tile = (function () {
    function Tile(tile_type) {
        this.tileType = tile_type;
        this.width = 16;
        this.height = 16;
    }
    Tile.prototype.draw = function (ctx, x, y) {
        ctx.fillText(this.tileType, x * this.width, y * this.height);
    };
    return Tile;
}());
exports.Tile = Tile;
//# sourceMappingURL=tile.js.map