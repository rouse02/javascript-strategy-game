import { Camera } from "./camera";
import { Tile } from "./tile";

class TileMap{
    tileList: Array<Tile>;
    map: Array<Array<Tile>>;
    private width: number;
    private height: number;

    constructor(w: number, h: number){
        this.width = w;
        this.height = h;        
    }

    init(): void{
        for (var i = 0; i < this.height; i++){
            for (var j = 0; j < this.width; j++){
                this.map[i][j] = new Tile("a");
            }
        }
        console.log(this.map);
    }

    draw(ctx: CanvasRenderingContext2D, camera: Camera){
        for (var i = 0; i < this.height; i++){
            for (var j = 0; j < this.width; j++){
                if ((i >= camera.position.x)
                    && (j >= camera.position.y)){
                    this.map[i][j].draw(ctx, i, j);
                }
            }
        }
    }
}

export {TileMap};