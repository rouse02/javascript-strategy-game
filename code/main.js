"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tileMap_1 = require("./engine/tileMap");
var ViewPort = require("./engine/camera");
var appPerformance_1 = require("./helpers/appPerformance");
var Main = (function () {
    function Main() {
    }
    Main.prototype.init = function () {
        this.lastTime = Date.now();
        this.canvas = document.getElementById("game");
        this.ctx = this.canvas.getContext("2d");
        this.fpsTracker = new appPerformance_1.AppPerformance(document.getElementById("fps"));
        this.screen = new ViewPort.Camera(this.canvas.width, this.canvas.height);
        this.map = new tileMap_1.TileMap(this.canvas.width, this.canvas.height);
        this.map.init();
    };
    Main.prototype.main = function () {
        var _this = this;
        var now = Date.now();
        var dt = (now - this.lastTime) / 1000.0;
        this.update(dt);
        this.render(this.ctx);
        this.fpsTracker.updateFps(dt);
        this.lastTime = now;
        requestAnimationFrame(function () { return _this.main(); });
    };
    Main.prototype.update = function (dt) {
        //TODO: Update logic
    };
    Main.prototype.render = function (ctxt) {
        ctxt.clearRect(0, 0, this.canvas.width, this.canvas.height);
        //put things to draw here... In FILO order.
    };
    return Main;
}());
exports.Main = Main;
//# sourceMappingURL=main.js.map