import { TileMap } from "./engine/tileMap";
import { Tile } from "./engine/tile";
import * as ViewPort from "./engine/camera";
import { Utility } from "./helpers/util";
import { AppPerformance } from "./helpers/appPerformance";

class Main {
    canvas: HTMLCanvasElement;
    ctx: CanvasRenderingContext2D;
    lastTime: number;
    map: TileMap;
    screen: ViewPort.Camera;
    fpsTracker: AppPerformance;

    init(): void{
        this.lastTime = Date.now();
        this.canvas = <HTMLCanvasElement>document.getElementById("game");
        this.ctx = this.canvas.getContext("2d");
        this.fpsTracker = new AppPerformance(document.getElementById("fps"));
        this.screen = new ViewPort.Camera(this.canvas.width, this.canvas.height);
        this.map = new TileMap(this.canvas.width, this.canvas.height);
        this.map.init();
    }

    main(): void{
        let now = Date.now();
        let dt = (now - this.lastTime) / 1000.0;

        this.update(dt);
        this.render(this.ctx);
        this.fpsTracker.updateFps(dt);
        this.lastTime = now;
        requestAnimationFrame(() => this.main());
    }

    private update(dt: number){
        //TODO: Update logic
    }

    private render(ctxt: CanvasRenderingContext2D): void{
        ctxt.clearRect(0,0,this.canvas.width, this.canvas.height);

        //put things to draw here... In FILO order.

    }
}

export {Main};