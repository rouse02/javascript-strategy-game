class AppPerformance{
    fpsElement: HTMLElement;

    constructor(output: HTMLElement){
        this.fpsElement = output;
    }
    
    updateFps(val: number): void{
        let t = 10 / val;
        this.fpsElement.innerHTML = "FPS: " + t.toFixed(1);
    }
}

export {AppPerformance};