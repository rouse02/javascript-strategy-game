class Utility {
    writeToConsole(input: string, isError: boolean = false): void{
        let t = document.getElementById("console_log");

        let result = "<p";
        result += isError ? " style='color: red'>" : ">";

        result += input.toString().replace("\n", "<br/>") + "</p>";
        let cur = t.innerHTML;
        
        t.innerHTML = (cur + result);
    }

    createArray(length): Array<any>{
        var arr = new Array(length || 0),
        i = length;

        if (arguments.length > 1) {
            var args = Array.prototype.slice.call(arguments, 1);
            while(i--) arr[length-1 - i] = this.createArray.apply(this, args);
        }

        return arr;
    }
}

export {Utility}