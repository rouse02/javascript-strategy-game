"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utility = (function () {
    function Utility() {
    }
    Utility.prototype.writeToConsole = function (input, isError) {
        if (isError === void 0) { isError = false; }
        var t = document.getElementById("console_log");
        var result = "<p";
        result += isError ? " style='color: red'>" : ">";
        result += input.toString().replace("\n", "<br/>") + "</p>";
        var cur = t.innerHTML;
        t.innerHTML = (cur + result);
    };
    Utility.prototype.createArray = function (length) {
        var arr = new Array(length || 0), i = length;
        if (arguments.length > 1) {
            var args = Array.prototype.slice.call(arguments, 1);
            while (i--)
                arr[length - 1 - i] = this.createArray.apply(this, args);
        }
        return arr;
    };
    return Utility;
}());
exports.Utility = Utility;
//# sourceMappingURL=util.js.map