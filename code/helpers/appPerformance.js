"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppPerformance = (function () {
    function AppPerformance(output) {
        this.fpsElement = output;
    }
    AppPerformance.prototype.updateFps = function (val) {
        var t = 10 / val;
        this.fpsElement.innerHTML = "FPS: " + t.toFixed(1);
    };
    return AppPerformance;
}());
exports.AppPerformance = AppPerformance;
//# sourceMappingURL=appPerformance.js.map