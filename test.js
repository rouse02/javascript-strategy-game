var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Menu = (function () {
    function Menu(item_list, total_pages) {
        this.items = item_list;
        this.pages = total_pages;
    }
    Menu.prototype.list = function () {
        console.log("Our menu for today:");
        for (var i = 0; i < this.items.length; i++) {
            console.log(this.items[i]);
        }
    };
    return Menu;
})();

var ExtendMenu = (function (_super) {
    __extends(ExtendMenu, _super);
    function ExtendMenu(item_list, total_pages) {
        _super.call(this, item_list, total_pages);
    }
    ExtendMenu.prototype.list = function () {
        console.log("Extending the menu Yo!");
        for (var i = 0; i < this.items.length; i++) {
            console.log("Extended: " + this.items[i]);
        }
    };
    return ExtendMenu;
})(Menu);

var FunctionHelpers = (function () {
    function FunctionHelpers() {
    }
    FunctionHelpers.prototype.genericFunc = function (argument) {
        var arrayOfT = [];
        arrayOfT.push(argument);
        return arrayOfT;
    };
    return FunctionHelpers;
})();

var sundayMenu = new Menu(["pan", "egg", "OJ"], 1);
var exMenu = new ExtendMenu(["pan", "egg", "OJ"], 1);
sundayMenu.list();
exMenu.list();
